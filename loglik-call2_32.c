#include <R.h>
#include <Rmath.h>
#include <Rinternals.h>
#include <math.h>
#include <stdio.h>


#define DIMS(x)							\
  INTEGER(coerceVector(getAttrib((x), R_DimSymbol), INTSXP))   \


long double factorial(long double initial,int n){   //compute initial*n!
    long double result=initial;
    for(int i=1;i<n+1;i++){
        result *= n;
            }
    if(result <0.0){
        printf("%s\n","overflow");
    }

    return result;
}

void count(double *prob, int nmut,int* nunsign, int* nc){//, double *loglik,int j){
    for(int i=0;i<nmut;i++){
        if((prob[i] > 1e-6) & (prob[i]< 1-(1e-6))){
            (*nunsign)++;
        }else if(prob[i]> 1-(1e-6)){
            (*nc)++;
        }
    }
}


void causind(int *rcausal,double *prob, int nmut ,int N) /*generate causal matrix for a sample with nmut mutaitons.nunde_mut:*/
{
    int num,j;
    num=N;
    for(j=0;j<nmut;j++){
        if(prob[j]<1e-6) {rcausal[j]=0;}
        else if(prob[j]>1-(1e-6) ) {rcausal[j]=1;}
        else{
        if(num>0){
            rcausal[j]=num % 2;
            num=num-rcausal[j];
            num /= 2;
        }else{
            rcausal[j]=0;
        }
     }
   }
}


long double conprob( int *rcausal, double *rprob_j, int nmut) /*caculate p(n_j|m_j),rcausal is the indicate of causal vec for a situation  （pnm is the output，rprob_j is the input probability）*/
{
    int j;
    double pnm;
    pnm=1.0;
    for(j = 0; j< nmut;j++){
      if(rcausal[j]==0){
      pnm *= (1-rprob_j[j]);
      }else{
        pnm *= rprob_j[j];
      }
    }
    return(pnm);
}

void caustable(int *rcausal, int *rY, int *newrY_j, int i, int nY0, int nY1)/*casual mut table of sample j corresponding to causal matrix(ncausal=2^n) */
/* rcausal: causal vector; rY_j: mut vector for jth sample; nY_j: length of rY_j ; newrY_j: output; */
{
  int index=0;
    for(int j=0; j < nY1; j++){
        newrY_j[j]=0;   
   for(int k=0;k<rY[i+j*nY0];k++){
        newrY_j[j] += rcausal[index];
        index += 1;
      }  
    }
}


long double sum(double *P, int *a, int *nP, int *na)
{
    int i, j;
    long double tmp, sum;
    sum = 0.0;
    for (i = 0; i < na[0]; i++) {
        tmp = 1.0;
        for (j = 0; j < na[1]; j++)
            tmp *= P[a[i + j*na[0]] - 1 + j*nP[0]];
        sum += tmp;
    }
    return sum;
}

long double partsum(double *P, int *a, int *nP, int *na, int beginrow, int endrow)  //caculate part some of multiplication of probability values correspoing to A matrix from beginrow to endrow.
{
    int i, j;
    long double tmp, sum;
    sum = 0.0;
    for (i = beginrow-1; i < endrow; i++) {
        tmp = 1.0;
        for (j = 0; j < na[1]; j++)
            tmp *= P[a[i + j*na[0]] - 1 + j*nP[0]];
        sum += tmp;
    }
    return sum;
}



  
long double logsum(double *P, int *a, int *nP, int *na)
{
  int i, j;
  long double tmp, sum;
  sum = 0.0;
  for (i = 0; i < na[0]; i++) {
    tmp = 1.0;
    for (j = 0; j < na[1]; j++) 
      tmp *= P[a[i + j*na[0]] - 1 + j*nP[0]];
    sum += tmp;
  }
  return log(sum);
} 

double dvecsum(double *x, int n)
{
  int i;
  double sum = 0.0;
  for (i = 0; i < n; i++)
    sum += x[i];
  return sum;    
}

int rowsum(int *x, int nx){     //sum of a vector x, nx is the length of x
    int i,sum=0;
    for (i = 0; i < nx; i++) {
        sum += x[i];
    }
    return sum;
}

int irowsum(int *x, int* nx, int row)
{
  int i, sum = 0;
  for (i = 0; i < nx[1]; i++) {
    sum += x[row + i*nx[0]];
  }
  return sum;    
}

void dsubmat(double *old, double *new, int *nold, int *nnew,
	     int* rowold, int* colold)
{
  int i, j;
  if (rowold != NULL && colold == NULL) {
    for (i = 0; i < nnew[0]; i++)
      for (j = 0; j < nnew[1]; j++)
	new[i + j*nnew[0]] = old[rowold[i] + j*nold[0]];
  } else if (rowold == NULL && colold != NULL) {
    for (i = 0; i < nnew[0]; i++)
      for (j = 0; j < nnew[1]; j++)
	new[i + j*nnew[0]] = old[i + colold[j]*nold[0]];
  } else if (rowold != NULL && colold != NULL) {
    for (i = 0; i < nnew[0]; i++)
      for (j = 0; j < nnew[1]; j++)
	new[i + j*nnew[0]] = old[rowold[i] + colold[j]*nold[0]];
  } else {
    for (i = 0; i < nnew[0]; i++)
      for (j = 0; j < nnew[1]; j++)
	new[i + j*nnew[0]] = old[i + j*nold[0]];
  }
}

void isubmat(int *old, int *new, int *nold, int *nnew,
	     int* rowold, int* colold)
{
  int i, j;
  if (rowold != NULL && colold == NULL) {
    for (i = 0; i < nnew[0]; i++)
      for (j = 0; j < nnew[1]; j++)
	new[i + j*nnew[0]] = old[rowold[i] + j*nold[0]];
  } else if (rowold == NULL && colold != NULL) {
    for (i = 0; i < nnew[0]; i++)
      for (j = 0; j < nnew[1]; j++)
	new[i + j*nnew[0]] = old[i + colold[j]*nold[0]];
  } else if (rowold != NULL && colold != NULL) {
    for (i = 0; i < nnew[0]; i++)
      for (j = 0; j < nnew[1]; j++)
	new[i + j*nnew[0]] = old[rowold[i] + colold[j]*nold[0]];
  } else {
    for (i = 0; i < nnew[0]; i++)
      for (j = 0; j < nnew[1]; j++)
	new[i + j*nnew[0]] = old[i + j*nold[0]];
  }
}


 SEXP loglik(SEXP x, SEXP AllPtrue, SEXP divsel,
	    SEXP Y, SEXP a, SEXP MAX,SEXP prob)
    
{
  int nx, ndivsel, *nAll, *nY, *nP, *na_N;
  int *rY, *rdivsel, *rMAX, *ra_N;
  int i, j, k, l, N,N1,c;

  double *rx, *rAllPtrue, *rans;
  double *AllP, *loglik;
  double *prob_i;
    long double pweight_c;
  //  int *newY_i;
    
  
 
  SEXP ans;

  PROTECT(ans = allocVector(REALSXP, 1));
  rans = REAL(ans);
  nx	 = length(x);
  ndivsel = length(divsel);
  nAll = DIMS(AllPtrue);
  nY	 = DIMS(Y);
  nP = (int *)R_alloc(2, sizeof(int));

  rx = REAL(x);
  rAllPtrue = REAL(AllPtrue);
  rdivsel = INTEGER(divsel); 
  rY = INTEGER(Y);
  rMAX = INTEGER(MAX);

   loglik = (double *)R_alloc(nY[0], sizeof(double));

  AllP = (double *)R_alloc(nAll[0]*nAll[1], sizeof(double));
  for (i = 0; i < nAll[0]; i++)
    for (j = 0; j < nAll[1]; j++)
      AllP[i + j*nAll[0]] = rAllPtrue[i + j*nAll[0]];
    
    double sumP=0.0;
  for(j=0; j<ndivsel ;j++)
     {
         for (i = 0; i < nAll[0] - 1; i++){
              AllP[i + (rdivsel[j] - 1)*nAll[0]] = exp(rx[i]);
              sumP += AllP[i + (rdivsel[j] - 1)*nAll[0]] ;
         }
     }
    
    sumP /= ndivsel;

    
    for(j=0; j<ndivsel ;j++)
    {
        for (i = 0; i < nAll[0] - 1; i++){
            AllP[i + (rdivsel[j] - 1)*nAll[0]] /= sumP+1;
            
        }
            AllP[nAll[0]-1 + (rdivsel[j]-1)*nAll[0]] = 1.0/(sumP+1);
    }

    
 //   for (i = 0; i < nAll[0] ; i++){
 //       printf("%f\t",AllP[i + (rdivsel[0] - 1)*nAll[0]]);
 //       }
  //  printf("\n");

 /*
       for(j=0; j<nAll[0] ;j++)
    {
        for (int k = 0; k < nAll[1]; k++){
           printf("%f\t",AllP[j + k*nAll[0]]);
        }
        printf("\n");
    }  */


 /*  for(j=0; j<ndivsel ;j++)
    {
      for (i = 0; i < nAll[0] - 1; i++)
	AllP[i + (rdivsel[j] - 1)*nAll[0]] = rx[i];
      AllP[i + (rdivsel[j]-1)*nAll[0]] = 1 - dvecsum(rx, nx);
    }
 */
    

  for (i = 0; i < nY[0]; i++) {
      loglik[i]=0.0;
      N = irowsum(rY, nY, i);
      prob_i=REAL(VECTOR_ELT(prob,i));
      int nc=0;
      int nunsign=0;
      count(prob_i,N,&nunsign,&nc);
      
      for(c=0;c<pow(2,nunsign);c++){
          int rcausal[N];
          causind(&rcausal[0],prob_i,N,c);
          pweight_c=conprob(&rcausal[0],prob_i,N);
          
          int newY_i[nY[1]];
          caustable(rcausal, rY , newY_i, i, nY[0],nY[1] );
          
          N1=rowsum(&newY_i[0],nY[1]);
          if(N1==0){
              loglik[i] += pow(1.0/nY[1],N)*pweight_c;
          }else{
              nP[0] = N1;
              nP[1] = N1;
              double P[nP[0]*nP[1]];
              int rowAll[nP[0]];
          
              l = 0;
              for (j = 0; j < nY[1]; j++)
                  for (k = 0; k < newY_i[j]; k++) {
                      rowAll[l++] = j;
                  }
          
              dsubmat(AllP, &P[0], nAll, nP, &rowAll[0], NULL);
          
              na_N = DIMS(VECTOR_ELT(a, N1-1));
              ra_N = INTEGER(VECTOR_ELT(a, N1-1));
              
              long double temp1=sum(P, ra_N, nP, na_N)*pweight_c*pow(1.0/nY[1],N-N1);
              long double temp=factorial(temp1,N1-rMAX[0]);
              
     //         double temp = temp1*temp2*temp3;//sum(P, ra_N, nP, na_N)*pweight_c*pow(1.0/nY[1],N-N1)*factorial(N1-rMAX[0]);
              if(temp<0.0){
                  printf("%d\t",i);
                  printf("%d\t",c);
                  printf("%Lf\t",temp1);
                  printf("%d\t",N1-rMAX[0]);
              }
              loglik[i] += temp;
  //            loglik[i] += sum(P, ra_N, nP, na_N)*pweight_c*pow(1.0/nY[1],N-N1)*factorial(N1-rMAX[0]);
          }
      }
/*      if(loglik[i]<0.0){
      printf("%d\n",i);
      printf("%f\n",loglik[i]);
      } */
      
 loglik[i]=log(loglik[i]);

      }
  
    
  *rans = -dvecsum(loglik, nY[0]);

  UNPROTECT(1);

  return ans;
}

void chindex(int* seq, int nseq, int a, int omit)
{
  int i, j;

  j = 0;
  for (i = 0; i < nseq; i++) {
    if (i == omit)
      j++;
    seq[i] = j + a;
    j++;
  }
}


void makeimat(int *old, int *new, int *no, int *nn,
	      int radd, int cadd, int romit, int comit)
{
  int i, j, r, c;
  for (i = 0, r = 0; i < nn[0]; i++, r++) {
    if (i == romit)
      r++;
    for (j = 0, c = 0; j < nn[1]; j++, c++) {
      if (j == comit)
	c++;
      new[i + j*nn[0]] = old[r + radd + (c + cadd)*no[0]];
    }
  }
}

void rowreduce(double *x, int nx)
{
  int i;
  for (i = 0; i < nx; i++)
    x[i] -= x[nx-1];
}

SEXP predict(SEXP Yj, SEXP AllPesti, SEXP a,SEXP prob,SEXP MAX)  //function to caculate P(A<B),P(A>B),P(A=B)

{
    int nYj, *na_N,*nAll;
    int *rYj,*rMAX, *ra_N;
    int N,N1;
    
    double *rAllPesti, *rans, *rprob;
    long double pweight_c;
    
    SEXP ans;
    PROTECT(ans = allocVector(REALSXP, 3)); //Probability of P(A=B),P(A<B),P(A>B)
    rans = REAL(ans);

//    probAB=(double *)R_alloc(3,sizeof(double));
//    for(int i=0;i<3;i++){
//        probAB[i]=0.0;
//    }
    
    nYj	 = length(Yj);
    rYj = INTEGER(Yj);
    N=rowsum(rYj,nYj);
    nAll = DIMS(AllPesti);
    rMAX=INTEGER(MAX);
    int nP[2];
    
    rAllPesti = REAL(AllPesti);
    rprob=REAL(prob);
    
    int nunsign=0;
    int nc=0;
    count(rprob,N,&nunsign,&nc);
    
    
    
    for(int c=1;pow(2,nunsign);c++){
        int rcausal[N];
        causind(&rcausal[0],rprob,N,c);
        pweight_c=conprob(&rcausal[0],rprob,N);
        int newYj[nYj];
        caustable(rcausal, rYj , newYj, 1, 1,nYj);
        
        N1=rowsum(&newYj[0],nYj);   //N1: number of causal mut corresponding to c
        
        nP[0] = N1;
        nP[1] = N1;
        double P[nP[0]*nP[1]];
        int rowAll[nP[0]];
            
        int l = 0;
        for (int j = 0; j < nYj; j++)
            for (int k = 0; k < newYj[j]; k++) {
                rowAll[l++] = j;
            }
            
        dsubmat(rAllPesti, P, nAll, nP, rowAll, NULL);
        
        na_N = DIMS(VECTOR_ELT(a, N1-1));
        ra_N = INTEGER(VECTOR_ELT(a, N1-1));
        int arows[3];
        for( int i =0;i<3;i++)
            arows[i]=na_N[i]/N1*newYj[i];
        
        long double temp[3];
        
        temp[0]=partsum(P, ra_N, nP, na_N,1,arows[0])*pweight_c*pow(1.0/3,N-N1);
        temp[0]=factorial(temp[0],N1-rMAX[0]);
        temp[1]=partsum(P, ra_N, nP, na_N,arows[0],arows[1])*pweight_c*pow(1.0/3,N-N1);
        temp[1]=factorial(temp[1],N1-rMAX[0]);
        temp[2]=partsum(P, ra_N, nP, na_N,arows[1],arows[2])*pweight_c*pow(1.0/3,N-N1);
        temp[2]=factorial(temp[2],N1-rMAX[0]);
        
        double jointprob[3];  //P(Y_i,j intersection A<B)
       
        for (int i=0;i<3;i++ ) {
            jointprob[i] += temp[i];
        }
    }

    for( int i=0;i<3;i++){
        rans[i]=rans[i]*rAllPesti[i];
    }
    for( int i=0;i<3;i++){
        rans[i]=dvecsum(rans,3);
    }


    UNPROTECT(1);
    
    return ans;
}


/*
SEXP gradient(SEXP x, SEXP AllPtrue, SEXP divsel,
	      SEXP Y, SEXP a, SEXP MAX, SEXP prob,SEXP aa)
{
  int nx, ndivsel, nAll[2], nY[2], nP[2], nP1[2], na_N[2], na_N1[2], ngrad,N1;
  int *rY, *rdivsel, *rMAX, *ra_N, *ra_N1;
  int i, j, k, l, cnt, N;

  double *rx, *rAllPtrue, *rans;
  double *AllP, *lik, *grad,*prob_i,pweight_c;
 
  SEXP ans;

  nx = length(x);
  ndivsel=length(divsel);
  nAll[0] = DIMS(AllPtrue)[0]; 
  nAll[1] = DIMS(AllPtrue)[1];
  nY[0] = DIMS(Y)[0]; 
  nY[1] = DIMS(Y)[1];

  rx = REAL(x);
  rAllPtrue = REAL(AllPtrue);
  rdivsel = INTEGER(divsel);
  rY = INTEGER(Y);
  rMAX = INTEGER(MAX);

  lik = (double *)R_alloc(nY[0], sizeof(double));

  AllP = (double *)R_alloc(nAll[0]*nAll[1], sizeof(double));
  for (i = 0; i < nAll[0]; i++)
    for (j = 0; j < nAll[1]; j++)
      AllP[i + j*nAll[0]] = rAllPtrue[i + j*nAll[0]];
 

  for(j=0; j<ndivsel ;j++)
    {
      for (i = 0; i < nAll[0] - 1; i++)
	AllP[i + (rdivsel[j] - 1)*nAll[0]] = rx[i];
      AllP[i + (rdivsel[j]-1)*nAll[0]] = 1 - dvecsum(rx, nx);
    }

  ngrad = nY[1];
  grad = (double *)R_alloc(ngrad, sizeof(double));
  for (i = 0; i < ngrad; i++)
    grad[i] = 0.0;

  PROTECT(ans = allocVector(REALSXP, ngrad - 1));
  rans = REAL(ans);
    


  for (i = 0; i < nY[0]; i++) {
      
      lik[i]=0.0;
      N = irowsum(rY, nY, i);
      prob_i=REAL(VECTOR_ELT(prob,i));
      int nc=0;
      int nunsign=0;
      count(prob_i,N,&nunsign,&nc);
      
      double grad_i[ngrad];
    //  grad_i=(double *)R_alloc(ngrad, sizeof(double));
      for (int m = 0; m < ngrad; m++)
          grad_i[m] = 0.0;
      
      for(int c=0;c<pow(2,nunsign);c++){
          int rcausal[N];
          causind(&rcausal[0],prob_i,N,c);
          pweight_c=conprob(&rcausal[0],prob_i,N);
          
          int newY_i[nY[1]];
          caustable(rcausal, rY , newY_i, i, nY[0],nY[1] );
          
          N1=rowsum(&newY_i[0],nY[1]);
          if(N1==0){
              lik[i] += pow(1.0/nY[1],N)*pweight_c;
          }else{
              nP[0] = N1;
              nP[1] = N1;
              double P[nP[0]*nP[1]];
              int rowAll[nP[0]];
              
              l = 0;
              for (j = 0; j < nY[1]; j++)
                  for (k = 0; k < newY_i[j]; k++) {
                      rowAll[l++] = j;
                  }
              
              dsubmat(AllP, &P[0], nAll, nP, &rowAll[0], NULL);
              
              na_N[0] = DIMS(VECTOR_ELT(a, N1-1))[0];
              na_N[1] = DIMS(VECTOR_ELT(a, N1-1))[1];
              ra_N = INTEGER(VECTOR_ELT(a, N1-1));
              lik[i] += sum(P, ra_N, nP, na_N)*pweight_c*pow(1.0/nY[1],N-N1)*factorial(N1-rMAX[0]);


    if (rdivsel[0] <= N1) {

      if (N1 > 1) {


		
	nP1[0] = nP[0] - 1;
	nP1[1] = nP[1] - 1;
	//P1 = (double *)R_alloc(nP1[0]*nP1[1], sizeof(double));
	//rowP = (int *)R_alloc(nP1[0], sizeof(int));
	//colP = (int *)R_alloc(nP1[1], sizeof(int));
          double P1[nP1[0]*nP1[1]];
          int rowP[nP1[0]];
          int colP[nP1[0]];

	cnt = 0;
	for (j = 0; j < nP[0]; j++) {
	  cnt++;

	  for(l=rdivsel[0];l<=imin2(N,rdivsel[ndivsel-1]);l++)
	    {
	      chindex(rowP, nP1[0], 0, cnt - 1);
	      chindex(colP, nP1[1], 0, l - 1);
	      dsubmat(P, P1, nP, nP1, rowP, colP);

	      if (l>rMAX[0]) {

		na_N1[0] = DIMS(VECTOR_ELT(a, N1-2))[0];
		na_N1[1] = DIMS(VECTOR_ELT(a, N1-2))[1];
		ra_N1 = INTEGER(VECTOR_ELT(a, N1-2));
		grad_i[rowAll[j]] +=
		  //exp(logsum(P1, ra_N1, nP1, na_N1) - loglik[i])/(N-rMAX[0]);
              sum(P1, ra_N1, nP1, na_N1)*pweight_c*pow(1.0/nY[1],N-N1)*factorial(N1-rMAX[0]);

	      } else
		{
		  na_N1[0] = DIMS(VECTOR_ELT(aa, N1-2))[0];
		  na_N1[1] = DIMS(VECTOR_ELT(aa, N1-2))[1];
		  ra_N1 = INTEGER(VECTOR_ELT(aa, N1-2));
		  grad_i[rowAll[j]] +=
            sum(P1, ra_N1, nP1, na_N1)*pweight_c*pow(1.0/nY[1],N-N1)*factorial(N1-rMAX[0]) ;

		}  
	    }
	}
	rowreduce(grad_i, ngrad);

      } else {
	for (j = 0; j < nY[1]; j++)
	  if (rY[i + j*nY[0]] == 1)
	    grad[j] += (1./P[0])*pweight_c*pow(1.0/nY[1],N-N1);

	rowreduce(grad_i, ngrad);
      }
    }
    
    }
    }
    for (j=0;j< ngrad -1;j++){
        grad_i[j] /=lik[i];
        grad[j] += grad_i[j];
    }
    
  }
    
    for (i = 0; i < ngrad - 1; i++)
    rans[i] = -grad[i];
    
  UNPROTECT(1);
  return ans;
}

*/

