
loglik <- function( xx, AllPtrue, division, sel, Y, a, prob,aa)
{
    storage.mode(xx) <- "double"
    storage.mode(AllPtrue) <- "double"
    for (i in 1:length(division))
        storage.mode(division[[i]]) <- "integer"
    storage.mode(sel) <- "integer"
      storage.mode(Y) <- "integer"
    for (i in 1:length(a))
        storage.mode(a[[i]]) <- "integer"
    for (i in 1:length(prob))
      storage.mode(prob[[i]]) <- "double"
    storage.mode(MAX) <- "integer"
    divsel=division[[sel]]
    storage.mode(divsel) <- "integer"

    .Call("loglik", xx, AllPtrue, divsel, Y, a, MAX, prob)
}
gradient <- function( xx, AllPtrue, division, sel, Y, a,prob,aa)
{
    storage.mode(xx) <- "double"
    storage.mode(AllPtrue) <- "double"
    for (i in 1:length(division))
        storage.mode(division[[i]]) <- "integer"
    storage.mode(sel) <- "integer"
      storage.mode(Y) <- "integer"
    for (i in 1:length(a))
        storage.mode(a[[i]]) <- "integer"

    for (i in 1:length(aa))
        storage.mode(aa[[i]]) <- "integer"
    for (i in 1:length(prob))
      storage.mode(prob[[i]]) <- "double"

    storage.mode(MAX) <- "integer"
    divsel=division[[sel]]
    storage.mode(divsel) <- "integer"


    .Call("gradient", xx, AllPtrue, divsel, Y, a,MAX, prob ,aa)
}


hin <- function(x,AllP,division,sel,Y,a,prob,aa)     # a vector function specifying inequality constraints such that hin[j] > 0 for all j
{                                                # used in the constrained optimization function auglag
    return(c(x,1-sum(x)))
}

hin.jac <- function(x,AllP,division,sel,Y,a,prob,aa)   # Jacobian of hin, used in the constrained optimization function auglag
{
    return(rbind(diag(length(x)),rep(-1,length(x))))
}

subloglik <- function(P,N,a)
{
    temp <- rep(1,nrow(a[[N]]))
    for(j in 1:ncol(a[[N]]))
        temp <- temp*P[a[[N]][,j],j]

    return(log(sum(temp)))
}



main.function <- function(Y,a,aa,prob,cutoff,max_unfix)  #old Y#
{
    nomut <- max(rowSums(Y))
    no.gene <- ncol(Y)
    if(cutoff>0){
      for(i in 1:length(prob)){
        for(k in 1:length(prob[[i]])){
          if(prob[[i]][k]<=cutoff || prob[[i]][k]>=1-cutoff ) {prob[[i]][k]=round(prob[[i]][k])}
        }
        }
    }

    unfix=max(sapply(1:length(prob), function(i) sum(prob[[i]]!=0  & prob[[i]]!=1)))
    print(unfix)
    
    if(max_unfix!=0){
      for(i in 1:length(prob)){
        if(sum(prob[[i]]!=0 & prob[[i]]!=1)>max_unfix){
      sort.index=sort(abs(prob[[i]]-0.5),decreasing = T,index.return=T)[[2]]
      round.index=sort.index[1:(length(prob[[i]])-max_unfix)]
      prob[[i]][round.index]=round(prob[[i]][round.index]) 
        }
      }
    }
    
    max.dmut=max(sapply(1:length(prob), function(i) sum(prob[[i]]!=0 )))
    print(max.dmut)
    print(sum(sapply(1:length(prob), function(i) 2^sum(prob[[i]]!=0 & prob[[i]]!=1))))
    nparam <- min(MAX+1,max.dmut)
    division <- vector("list",nparam)
    for(i in 1:(nparam-1)) division[[i]] <- i
    division[[nparam]] <- nparam:max.dmut

#########generate causal list , new Y list, pweight list#########
 #   causal.list<-sapply(apply(Y,1,sum),generate.causal.fun)
    
 #   newY<-vector("list",length(causal.list))
#    for( i in 1:length(causal.list)){
#      mut.matr=generate.mut.matr(Y[i,],causal.list[[i]])
#      newY[[i]]<-mut.matr
#    }    
    
#    pweight<-vector("list",length(causal.list))
#    for(i in 1:length(causal.list)){
#      pweight[[i]]=calc.p_weight(score.list[[i]],causal.list[[i]],Y[i,])
#    }

###############################################################

    AllPtrue <- matrix(0,nr=no.gene,nc=max.dmut)


    for(j in 1:nparam)
    {
        init=runif(no.gene) ;init=init/sum(init)
        AllPtrue[,division[[j]]] <-  init
    }

    prev0=10^30
    prev=10^30
    n=0
    decrease=1

    no.repeat=20
    decrease.limit=10^(-6)

    while(decrease>decrease.limit & n<=no.repeat)
    {
        n=n+1
        prev0=prev              # For each k, the length of \vec{P_{k}} optimized is N-1 where N is the number of driver genes. This is because \sum_{i=1}^{N} P_{k,i} = 1 and the value of P_{k,N} is determeined by the other N-1 P_{k,i}, thus we optimize only for P_{k,i} for i=1,..N-1. we let P_{k,N}=1-\sum_{i=1}^{N-1}{P_{k,i}}
        for(sel in 1:nparam)    # find \vec{P_{k}} maximizing the log likelihood  for each k in turn with the constraint that 0<P_{k,i} for i=1,..N-1 and 0< 1-\sum_{i=1}^{N-1}{P_{k,i}}
        {    
            no.fail=0
            error=T
            while(no.fail<=5 & error){
            
            init=runif(no.gene-1,-5,5); #init=init/sum(init)  # initial values for \vec{P_{k}}
            
   #         u=exp(init)/(sum(exp(init))+1)
   #         u=c(u,1-sum(u))
   #         print(u)
            x <- try(optim(par=init,fn=loglik,control=list(maxit=10000),AllP=AllPtrue,division=division,sel=sel,Y=Y,a=a,aa=aa,prob=prob),silent=T)
   #         x <- try(auglag(par=init[-no.gene],fn=loglik,hin=hin,hin.jac=hin.jac,control.optim=list(maxit=10000),control.outer=list(trace=F),AllP=AllPtrue,division=division,sel=sel,Y=Y,a=a,aa=aa,prob=prob),silent=T)
            error=is(x,"try-error")
             if(is(x,"try-error")){
               print(x)
               print("loglik")
               print(loglik(init,AllPtrue,division,1,Y,a,prob,aa))
               print("AllPtrue")
               print(AllPtrue)
               print("init")
               print(init)
#              for(j in 1:nparam)
#              {
#                init=runif(no.gene); init=init/sum(init)
#                AllPtrue[,division[[j]]] <-  init
#              }
#              AllPtrue[,1]=init
#              return(AllPtrue)
               no.fail=no.fail+1
               
              }
            if(!is(x,"try-error")){
              print(sel)
              
              print(x$par)
   #         nonnegative <- pmax(c(x$par,1-sum(x$par)),0)
   #          tempAllPtrue[,division[[sel]]] <- nonnegative/sum(nonnegative)   # This is needed since sometimes the value x$par are negative.

    #        tempAllPtrue[-no.gene,division[[sel]]] <- x$par
    #        fval <- loglik(tempAllPtrue[-no.gene,1],tempAllPtrue,division,1,Y,a,prob,aa)     # negative log likelihood
    #         fval <- loglik(tempAllPtrue[-no.gene,division[[sel]][1]],tempAllPtrue,division,sel,Y,a,prob,aa)
            fval=x$value
            decrease=prev-fval
            
   #         print(n)
             print(decrease)
            

            
            if(decrease>0)          # if the negative log likelihood decreases, then change old P_{k} with new P_{k}
            {
                prev=fval
                AllPtrue[-no.gene,division[[sel]]] <- exp(x$par)/(sum(exp(x$par))+1)
                AllPtrue[no.gene,division[[sel]]] <- 1/(sum(exp(x$par))+1)
            }
            }
        }  
        }
        decrease=prev0-prev
        
    }
    
    
 #   print(AllPtrue)
   return(list(AllPtrue,prev))
}


generatea <- function(MAX,MAX.mut)
{
a <- vector("list",MAX.mut)
a[[1]] <- matrix(1,nr=1,nc=1)

for(N in 2:MAX.mut)
{
    x <- matrix(1:N,nr=N)
    for(i in 1:(min(N,MAX)-1))
    {
        y <- rep(1:N,nrow(x))[-as.vector(t(x+((1:nrow(x))-1)*N))]
        x <- t(matrix(as.vector(t(matrix(rep(as.vector(x),N-ncol(x)),nr=nrow(x)))),nr=ncol(x)))
        x <- cbind(x,y)
    }
    if(N>MAX)
    {
        y <- rep(1:N,nrow(x))[-as.vector(t(x+((1:nrow(x))-1)*N))]
        y <- t(matrix(y,nc=nrow(x)))
        x <- cbind(x,y)
    }
    a[[N]] <- x
}
return(a)
}



order_estimate <- function(sample.gene.mutation,N,parallel,score.list,cutoff=0,max_unfix=0)
{
  nomut <- max(rowSums(sample.gene.mutation))
  
   max.dmut=max(sapply(1:length(score.list), function(i) sum(score.list[[i]]!=0)))
#   a <- generatea(MAX,nomut)
#   aa <- generatea(MAX-1,nomut)
    a <- generatea(MAX,max.dmut)
    aa <- generatea(MAX-1,max.dmut)
    print("over")

## Run optimization with different inital values N times using parallel computing if the variable "parallel" is TRUE :
    if(parallel)
        tmp <- foreach (kk = 1:N) %dopar%  main.function(sample.gene.mutation,a,aa,score.list,cutoff,max_unfix)
    else ## Otherwise, run for loop
    {
        tmp <- vector("list", N)
        for(i in 1:N) tmp[[i]] <- main.function(sample.gene.mutation,a,aa,score.list,cutoff,max_unfix)
    }

    minusloglik=rep(Inf,N)
    for(l in 1:N)
        if(is.list(tmp[[l]])) minusloglik[l]=tmp[[l]][[2]]

    result <- tmp[[which(minusloglik==min(minusloglik))[1]]][[1]] #find the one giving the maximum likelihood

    return(result)

}



total <- function(X) # Prob that there is any mutation in a gene
    return(1-apply(1-X,1,prod))


Intersection <- function(X,N)
{
    sel <- combinations(ncol(X),N)
    res <- 0
    for(i in 1:nrow(sel))
        res <- res+apply(X[,sel[i,]],1,prod)
    return(res)
}
Union <- function(X)
{
    res <- 0
    for(i in 2:ncol(X))
        res <- res+Intersection(X,i)*(-1)^(i+1)
    return(apply(X,1,sum)+res)
}

# constructing BCa CI from bootstrapped data for each of the colon and lung data
ConfInterval <- function(x,mle,jack,sample.gene.mutation,lower,upper)
{
    no.gene=ncol(sample.gene.mutation)

    z0 <- matrix(0,nr=no.gene,nc=ncol(mle))       # bias-correction constant

    for(i in 1:no.gene)
    {
        for(j in 1:ncol(mle))
        {
            z0[i,j] <- qnorm(mean(x[i,j,]<=mle[i,j],na.rm=T))
        }
    }

    z0[z0==Inf]=1000

    theta.mean <- apply(jack,1:2,mean,na.rm=T)

    num <- denom <- 0

    for(i in 1:nrow(sample.gene.mutation))
    {
        if(sum(is.na(jack[,,i]))==0)
        {
            num <- num+(theta.mean-jack[,,i])^3
            denom <- denom+(theta.mean-jack[,,i])^2
        }
    }
    accel <- num/6/denom^1.5   # acceleration constant

    alpha1 <- pnorm(z0+(z0+qnorm(lower))/(1-accel*(z0+qnorm(lower))))
    alpha2 <- pnorm(z0+(z0+qnorm(upper))/(1-accel*(z0+qnorm(upper))))

    CI=array(0,dim=c(no.gene,2,ncol(mle)))

    for(i in 1:no.gene)
    {
        for(j in 1:ncol(mle))
        {
            CI[i,1,j] <- quantile(x[i,j,],alpha1[i,j],na.rm=T)
            CI[i,2,j] <- quantile(x[i,j,],alpha2[i,j],na.rm=T)
        }
    }
    CI=round(CI,2)
    a=round(mle,2)

    tab =NULL
    for(i in 1:ncol(a))
    {
        tab=cbind(tab,"&",a[,i],"&","(" ,CI[,1,i], ",",CI[,2,i],")")
    }
    tab=cbind(colnames(sample.gene.mutation),tab,"\\")   # for generating tables in the paper

    return(tab)
}


generate.causal.fun=function(nmut){  
  causal=matrix(0,nrow=2^nmut,ncol=nmut)
  index=1
  for(i in 1:nmut){
    temp=combn(nmut,i)
    for(j in 1:ncol(temp)){
      index = index+1
      causal[index,temp[,j]]=1
    }
  }
  return(causal)
}



generate.mut.matr=function(origin.mut,causal.matrix){
  mut.matr=matrix(0,nrow=nrow(causal.matrix),ncol=length(origin.mut))
  for(i in 2:nrow(causal.matrix)){
    index=1
    for(j in 1:length(origin.mut)){
      if(origin.mut[j]>0){
        for(k in 1:origin.mut[j]){
          #            print(index)
          mut.matr[i,j]=mut.matr[i,j]+causal.matrix[i,index]
          index = index + 1
        }
      }
    }
  }
#  result=as.matrix(mut.matr[-1,])
#  if(dim(result)[2]==1){result=t(result)}
  return(mut.matr)
}

calc.p_weight=function(score,causal.matrix,origin.mut){
  n.gene=length(origin.mut)
  n.mut=sum(origin.mut)
  p=rep(NA,nrow(causal.matrix))
  for(i in 1:nrow(causal.matrix)){
    y.index=which(causal.matrix[i,]==1)
    n.index=which(causal.matrix[i,]==0)
    p[i]=1
    if(length(y.index>0)){
      p[i] = p[i]*prod(score[y.index],na.rm=T)
    }
    if(length(n.index>0)){
      p[i] = p[i]*prod(1-score[n.index],na.rm=T)
    }
    p[i]=p[i]*(1/n.gene)^length(n.index)
  }
  return(p)
}


